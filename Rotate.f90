program main
  implicit none
  real(kind=8), allocatable :: r(:,:)
  real(kind=8), allocatable :: rotmat(:,:)
  integer :: i, nat
  real(kind=8) :: angx,angy,angz,row(3)
  real(kind=8) :: thetax,thetay,thetaz
  character(len=1) :: char
   



!read in No of Atoms 
!  write(*,*) 'Reading in coordinates from coord.xyz'
  open(1,file='coord.xyz')
  read(1,*) nat

!allocate memory for coordinates and forces
  allocate(r(3,nat))

!Read in atoms x y z
  read(1,*)
  do i=1,nat
    read(1,*) char, r(:,i)
  end do
  close(1)

thetax=90  !X rotation in degrees
thetay=90  !Y rotation in degrees 
thetaz=90  !Z rotation in degrees

angx = 0.0174533*thetax
angy = 0.0174533*thetay
angz = 0.0174533*thetaz

!allocate memory for rotation matrix
allocate(rotmat(3,3))

!Rotate around X axis
 rotmat(1,1)=1.0
 rotmat(2,1)=0.0
 rotmat(3,1)=0.0

 rotmat(1,2)=0.0
 rotmat(2,2)=cos(angx)
 rotmat(3,2)=-1*sin(angx)

 rotmat(1,3)=0.0
 rotmat(2,3)=sin(angx)
 rotmat(3,3)=cos(angx)

!Rotate around Y axis
 rotmat(1,1)=cos(angy)
 rotmat(2,1)=0.0
 rotmat(3,1)=sin(angy)

 rotmat(1,2)=0.0
 rotmat(2,2)=1.0
 rotmat(3,2)=0.0

 rotmat(1,3)=-1*sin(angy)
 rotmat(2,3)=0.0
 rotmat(3,3)=cos(angy)


!Rotation around Z axis
 rotmat(1,1)=cos(angz)
 rotmat(2,1)=-1*sin(angz)
 rotmat(3,1)=0.0

 rotmat(1,2)=sin(angz)
 rotmat(2,2)=cos(angz)
 rotmat(3,2)=0.0

 rotmat(1,3)=0.0
 rotmat(2,3)=0.0
 rotmat(3,3)=1.0


write(*,*) nat
write(*,*)

 do i=1,nat
     row(1) =r(1,i)
     row(2) =r(2,i)
     row(3) =r(3,i)

     row =matmul(rotmat,row)
    
     r(1,i)   = row(1)
     r(2,i)   = row(2) 
     r(3,i)   = row(3)
     write(*,*) 'C', r(1,i),r(2,i),r(3,i)
 enddo 
!write(*,*) 'Rotate'

!write(*,*) cos(3.14)

end program main
